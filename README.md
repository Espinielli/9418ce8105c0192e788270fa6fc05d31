
TODOs:

* position labels for negative bars on the right side, see this [SO post](http://stackoverflow.com/a/11354023/963575)
* highlight relevant underlying bar when hovering on y-axis label, see commented out `stroke` styling on bars

Data about percent change of flights movement by entity (Jan - Feb 2016 vs Jan - Feb 2015) from [PRU website][2].


Inspired by Mike's [Bar Chart with Negative Values][1].

[1]: http://bl.ocks.org/mbostock/2368837
[2]: http://ansperformance.eu/graphs/traffic/
